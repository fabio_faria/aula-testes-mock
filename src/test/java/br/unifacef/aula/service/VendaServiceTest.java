package br.unifacef.aula.service;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.aula.dao.VendaDAO;
import br.unifacef.aula.dto.Venda;

@RunWith(MockitoJUnitRunner.class)
public class VendaServiceTest {

	@Mock
	private VendaDAO vendaDAO;
	
	@Test
	public void deveriaSalvarUmaNovaVenda() {
		
		Venda vendaEsperadaMock = new Venda();
		vendaEsperadaMock.setId(1234);
		vendaEsperadaMock.setVendedor("Joazinho");
		vendaEsperadaMock.setValor(new BigDecimal(1999));
		
		Venda novaVenda = new Venda();
		novaVenda.setValor(new BigDecimal(2000));
		novaVenda.setVendedor("Fabio");
		
		Mockito.when(vendaDAO.salvarVenda(novaVenda)).thenReturn(vendaEsperadaMock);
		
		VendaService vendaService = new VendaService(vendaDAO);
			
		Venda vendaRegistrada = vendaService.salvarVenda(novaVenda);
		
		System.out.println(vendaRegistrada);
		assertNotNull(vendaRegistrada);
		assertNotNull(vendaRegistrada.getId());
	}
}
