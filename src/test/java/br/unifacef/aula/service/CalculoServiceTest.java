package br.unifacef.aula.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculoServiceTest {

	private CalculoService calculoService;
	
	@Before
	public void init() {
		this.calculoService = new CalculoService();
	}
	
	@Test
	public void deveSomarPositivos() {
		
		Integer valorEsperdo = 15;
				
		assertEquals(valorEsperdo, this.calculoService.somar(7, 8));
	}
	
	@Test
	public void deveSomarNumerosInteiros() {
		
		Integer valorEsperdo = 15;
				
		assertEquals(valorEsperdo, this.calculoService.somar(20, -5));
	}
	
	@Test
	public void deveRetornarMaiorValorQuandoPrimeiroMaiorSegundo() {
		int a = 5;
		int b = 1;
		assertEquals(new Integer(5), this.calculoService.maior(a, b));
	}
	
	@Test
	public void deveRetornarMaiorValorQuandoPrimeiroMenorSegundo() {
		int a = -1;
		int b = 10;
		assertEquals(new Integer(10), this.calculoService.maior(a, b));
	}
	
	@Test
	public void deveRetornarZeroQuandoIgualdades() {
		int a = 10;
		int b = 10;
		assertEquals(new Integer(0), this.calculoService.maior(a, b));
	}
}
