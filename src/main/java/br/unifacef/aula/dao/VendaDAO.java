package br.unifacef.aula.dao;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import br.unifacef.aula.dto.Venda;

public class VendaDAO {

	/**
	 * M�todo que dever� salvar uma nova venda
	 * @param venda dados da venda a serem inclusos
	 * @return objeto persistido de venda
	 */
	public Venda salvarVenda(final Venda venda) {
		
		if(venda != null && venda.getId() == null) {
			venda.setId( new Double(Math.random() * 1000).intValue());
		}
		
		return venda;
	}
	
	public List<Venda> buscarVendaPorVendedor(String nome) {
		
		Venda v1 = new Venda();
		v1.setId(1);
		v1.setVendedor(nome);
		v1.setValor(new BigDecimal(5000));
		

		Venda v2 = new Venda();
		v2.setId(166);
		v2.setVendedor(nome);
		v2.setValor(new BigDecimal(2590));
		
		return Arrays.asList(v1, v2);
	}
}
